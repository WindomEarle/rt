// ===================================================================================================
//                                            CONSTANTS
// ===================================================================================================
const std = @import("std");
const warn = std.debug.warn;
usingnamespace @import("win32/everything.zig");
const assert = std.debug.assert;
const direct = std.heap.DirectAllocator;
const rand = std.rand;
const sqrt = std.math.sqrt;
const pow = std.math.pow;

const WIDTH = 1200;
const HEIGHT = 600;
const PI: f32 = 3.14159265;
// ===================================================================================================
//                                             STRUCTS
// ===================================================================================================
const WindowRect = struct {
    width: i32,
    height: i32,
};

const FrameBuffer = struct {
    width: i32,
    height: i32,
    info: BITMAPINFO,
    pixels: []u8,
};

const RenderContext = struct {
    start: usize,
    end: usize,
    world: HitableList(Sphere),
    camera: Camera,
    frame_width: usize,
    frame_height: usize,
    is_done: bool,
    random_seed: u8,
};

// ===================================================================================================
//                                             GLOBALS
// ===================================================================================================

var GlobalDirectAllocator = direct.init();
var GlobalFrameBuffer: FrameBuffer = undefined;
var GlobalRenderJobs: []RenderContext = undefined;
var GlobalLMouseDown: bool = false;
var GlobalRandomNumberGenerator: std.rand.Random = rand.DefaultPrng.init(0).random;

// ===================================================================================================
//                                             SYSTEM
// ===================================================================================================

fn rnd() f32 {
    return GlobalRandomNumberGenerator.float(f32);
}

export fn WindowCallback(hwnd: HWND, message: u32, wparam: WPARAM, lparam: LPARAM) LRESULT {
    var result: i32 = 0;
    switch (message) {
        WM_DESTROY => {
            PostQuitMessage(0);
        },
        WM_CLOSE => {
            _ = DestroyWindow(hwnd);
        },
        WM_SIZE => {
            update_frame(hwnd);
            if (!GlobalLMouseDown) {
                _ = try ResizeBuffer(GetWindowDimension(hwnd));
            }
        },
        WM_LBUTTONDOWN => {
            GlobalLMouseDown = true;
        },
        WM_LBUTTONUP => {
            GlobalLMouseDown = false;
        },
        WM_PAINT => {
            update_frame(hwnd);
        },
        else => result = @intCast(i32, DefWindowProcA(hwnd, message, wparam, lparam)),
    }
    return result;
}

fn update_frame(hwnd: HWND) void {
    var paint: PAINTSTRUCT = undefined;
    var dc = BeginPaint(hwnd, &paint);
    var rect = GetWindowDimension(hwnd);
    _ = StretchDIBits(
        dc,
        0,
        0,
        rect.width,
        rect.height,
        0,
        0,
        GlobalFrameBuffer.width,
        GlobalFrameBuffer.height,
        GlobalFrameBuffer.pixels.ptr,
        &GlobalFrameBuffer.info,
        DIB_RGB_COLORS,
        SRCCOPY, //winapi.SRCCOPY
    );
    _ = EndPaint(hwnd, &paint);
}

fn GetWindowDimension(hwnd: HWND) WindowRect {
    var rect = RECT{
        .left = 0,
        .top = 0,
        .right = 0,
        .bottom = 0,
    };
    _ = GetClientRect(hwnd, &rect);
    return WindowRect{
        .width = rect.right - rect.left,
        .height = rect.bottom - rect.top,
    };
}

fn ResizeBuffer(rect: WindowRect) !void {
    if (!is_rendering_done()) {
        return;
    }
    GlobalDirectAllocator.allocator.free(GlobalRenderJobs);
    var size: usize = @intCast(usize, 4 * rect.width * rect.height);
    GlobalDirectAllocator.allocator.free(GlobalFrameBuffer.pixels);
    var pixels = std.ArrayList(u8).init(&GlobalDirectAllocator.allocator);
    try pixels.resize(size);

    GlobalFrameBuffer.pixels = pixels.toOwnedSlice();
    GlobalFrameBuffer.info.bmiHeader.biSize = @sizeOf(winapi.BITMAPINFOHEADER);
    GlobalFrameBuffer.info.bmiHeader.biWidth = rect.width;
    GlobalFrameBuffer.info.bmiHeader.biHeight = rect.height;
    GlobalFrameBuffer.info.bmiHeader.biPlanes = 1;
    GlobalFrameBuffer.info.bmiHeader.biBitCount = 32;
    GlobalFrameBuffer.info.bmiHeader.biCompression = 0;
    GlobalFrameBuffer.width = rect.width;
    GlobalFrameBuffer.height = rect.height;

    _ = RenderMultithreaded();
}

pub fn main() u8 {
    var wndClass = winapi.Map.Ascii().WNDCLASS{
        .style = winapi.CS_VREDRAW | winapi.CS_HREDRAW,
        .lpfnWndProc = WindowCallback,
        .hInstance = winapi.GetModuleHandleA(null),
        .lpszClassName = "OutputWindow",
        .hCursor = winapi.LoadCursorA(null, winapi.Map.Ascii().IDC_CROSS),
        .cbClsExtra = 0,
        .cbWndExtra = 0,
        .hIcon = null,
        .hbrBackground = null,
        .lpszMenuName = null,
    };

    assert(winapi.RegisterClassA(&wndClass) != 0);
    GlobalFrameBuffer = FrameBuffer{
        .width = WIDTH,
        .height = HEIGHT,
        .info = undefined,
        .pixels = undefined,
    };

    var wHandle = winapi.CreateWindowExA(
        0,
        wndClass.lpszClassName,
        "Output window",
        winapi.WS_OVERLAPPEDWINDOW,
        winapi.CW_USEDEFAULT,
        winapi.CW_USEDEFAULT,
        WIDTH,
        HEIGHT,
        null,
        null,
        winapi.GetModuleHandleA(null),
        null,
    );
    assert(wHandle != null);

    _ = winapi.ShowWindow(wHandle, winapi.SW_SHOW);
    _ = winapi.UpdateWindow(wHandle);
    var message: winapi.MSG = undefined;
    while (winapi.PeekMessageA(&message, wHandle, 0, 0, winapi.PM_REMOVE) > 0) {
        _ = winapi.TranslateMessage(&message);
        _ = winapi.DispatchMessageA(&message);
        _ = winapi.InvalidateRect(wHandle, null, winapi.TRUE);
    }
    return 0;
}

// ===================================================================================================
//                                            RENDERING
// ===================================================================================================

fn RenderMultithreaded() !void {
    const cpu_count = try std.os.cpuCount(&GlobalDirectAllocator.allocator);
    const num_threads = cpu_count;
    const job_size = @intCast(usize, GlobalFrameBuffer.height) / num_threads;
    const job_remainder = @intCast(usize, GlobalFrameBuffer.height) - job_size * num_threads;

    var render_jobs = std.ArrayList(RenderContext).init(&GlobalDirectAllocator.allocator);
    _ = render_jobs.resize(num_threads);
    GlobalRenderJobs = render_jobs.toOwnedSlice();

    warn("\n");
    warn("==========================================================\n");
    warn("=                 Performing render job:                 =\n");
    warn("==========================================================\n");
    warn("    Total scanlines:          {}\n", GlobalFrameBuffer.height);
    warn("    CPU count:                {}\n", cpu_count);
    warn("    Render job size:          {}\n", job_size);
    warn("    Additional job size:      {}\n", job_remainder);
    warn("==========================================================\n");

    // Prepare threads
    var threads = std.ArrayList(*std.os.Thread).init(&GlobalDirectAllocator.allocator);
    defer threads.deinit();

    var cpu_job_index: usize = 0;
    var job_start: usize = 0;

    // Prepare world
    var aspect = @intToFloat(f32, GlobalFrameBuffer.width) / @intToFloat(f32, GlobalFrameBuffer.height);
    var lookfrom = vec3(13, 2, 3);
    var lookat = vec3(0, 0, 0);
    var dist_to_focus: f32 = 10.0;
    var aperture: f32 = 1.0;
    var camera = Camera.new(lookfrom, lookat, vec3(0, 1, 0), 20.0, aspect, aperture, dist_to_focus);
    var world = random_scene();

    while (cpu_job_index < num_threads) : (cpu_job_index += 1) {
        var job_adder = job_size;
        if (cpu_job_index == (num_threads - 1)) {
            job_adder += job_remainder;
        }

        var render_context = RenderContext{
            .start = job_start,
            .end = job_start + job_adder,
            .world = world,
            .camera = camera,
            .frame_width = @intCast(usize, GlobalFrameBuffer.width),
            .frame_height = @intCast(usize, GlobalFrameBuffer.height),
            .is_done = false,
            .random_seed = @intCast(u8, cpu_job_index),
        };
        warn("Render job: {}, start: {}, end: {}\n", cpu_job_index, render_context.start, render_context.end);

        // Launch thread job
        GlobalRenderJobs[cpu_job_index] = render_context;
        _ = threads.append(try std.os.spawnThread(&GlobalRenderJobs[cpu_job_index], DrawingRoutine));

        job_start += job_size;
    }
}

fn DrawingRoutine(render_context: *RenderContext) void {
    const samples_count = 500;
    var y: usize = render_context.start;
    var x: usize = 0;
    var H = render_context.end;
    var W = render_context.frame_width;

    while (y < H) : (y += 1) {
        while (x < W) : (x += 1) {
            var index: usize = 4 * (y * W + x);

            var col = vec3(0.0, 0.0, 0.0);
            var s: i32 = 0;
            while (s < samples_count) : (s += 1) {
                var u = (@intToFloat(f32, x) + rnd()) / @intToFloat(f32, W);
                var v = (@intToFloat(f32, y) + rnd()) / @intToFloat(f32, render_context.frame_height);
                var r = render_context.camera.get_ray(u, v);
                col = col.add(color(&r, &render_context.world, 0));
            }

            col = col.mulScalar(1.0 / @intToFloat(f32, samples_count));

            // Gamma correction of power 1/gamma = 1/2
            col = vec3(sqrt(col.x), sqrt(col.y), sqrt(col.z));

            GlobalFrameBuffer.pixels[index + 0] = @floatToInt(u8, 255.99 * col.z); // B
            GlobalFrameBuffer.pixels[index + 1] = @floatToInt(u8, 255.99 * col.y); // G
            GlobalFrameBuffer.pixels[index + 2] = @floatToInt(u8, 255.99 * col.x); // R
            GlobalFrameBuffer.pixels[index + 3] = 0; // A
        }
        x = 0;
    }

    render_context.is_done = true;
}

fn color(r: *Ray, world: *HitableList(Sphere), depth: i32) Vec3f {
    const MAXFLOAT: f32 = 3.40282346638528859812e+38;
    const DEPTH_LIMIT: i32 = 50;
    var rec: HitRecord = undefined;
    if (world.hit(r, 0.0001, MAXFLOAT, &rec)) {
        var scattered: Ray = undefined;
        var attenuation: Vec3f = undefined;
        if (depth < DEPTH_LIMIT and rec.material.scatter(r, &rec, &attenuation, &scattered)) {
            return color(&scattered, world, depth + 1).mul_vector(attenuation);
        } else {
            return vec3(0, 0, 0);
        }
    } else {
        var unit_direction = r.direction().unit_vector();
        var t = 0.5 * (unit_direction.y + 1.0);
        return vec3(1.0, 1.0, 1.0).mulScalar(1.0 - t).add(vec3(0.5, 0.7, 1.0).mulScalar(t));
    }
}

fn is_rendering_done() bool {
    var result = true;
    if (GlobalRenderJobs.len == 0) {
        return true;
    }
    for (GlobalRenderJobs) |render_job| {
        result = result and render_job.is_done;
    }
    return result;
}

// ===================================================================================================
//                                               MATH
// ===================================================================================================

fn vec3(x: f32, y: f32, z: f32) Vec3f {
    return Vec3f{ .x = x, .y = y, .z = z };
}

const Vec3f = Vec3(f32);

fn Vec3(comptime T: type) type {
    return struct {
        const Self = @This();
        x: T,
        y: T,
        z: T,

        fn mul(u: Self, v: Self) T {
            return u.x * v.x + u.y * v.y + u.z * v.z;
        }

        fn mul_vector(u: Self, v: Self) Self {
            return vec3(u.x * v.x, u.y * v.y, u.z * v.z);
        }

        fn mulScalar(u: Self, k: T) Self {
            return vec3(u.x * k, u.y * k, u.z * k);
        }

        fn add(u: Self, v: Self) Self {
            return vec3(u.x + v.x, u.y + v.y, u.z + v.z);
        }

        fn sub(u: Self, v: Self) Self {
            return vec3(u.x - v.x, u.y - v.y, u.z - v.z);
        }

        fn negate(u: Self) Self {
            return vec3(-u.x, -u.y, -u.z);
        }

        fn norm(u: Self) T {
            return sqrt(u.x * u.x + u.y * u.y + u.z * u.z);
        }

        fn normalize(u: Self) Self {
            return u.mulScalar(1 / u.norm());
        }

        fn cross(u: Self, v: Self) Self {
            return vec3(
                u.y * v.z - u.z * v.y,
                u.z * v.x - u.x * v.z,
                u.x * v.y - u.y * v.x,
            );
        }

        fn dot(u: Self, v: Self) T {
            return u.x * v.x + u.y * v.y + u.z * v.z;
        }

        fn unit_vector(u: Self) Self {
            return u.mulScalar(1 / u.norm());
        }

        fn squared_length(v: Self) T {
            return v.x * v.x + v.y * v.y + v.z * v.z;
        }

        fn reflect(v: Self, n: Vec3f) Self {
            return v.sub(n.mulScalar(2).mulScalar(v.dot(n)));
        }
    };
}

// ===================================================================================================
//                                            RAYTRACING
// ===================================================================================================

const Ray = struct {
    const Self = @This();
    A: Vec3f,
    B: Vec3f,

    pub fn new(a: Vec3f, b: Vec3f) Self {
        return Ray{
            .A = a,
            .B = b,
        };
    }

    pub fn origin(v: Self) Vec3f {
        return v.A;
    }

    pub fn direction(v: Self) Vec3f {
        return v.B;
    }

    pub fn point_at_parameter(v: Self, t: f32) Vec3f {
        return v.A.add(v.B.mulScalar(t));
    }
};

const HitRecord = struct {
    t: f32,
    p: Vec3f,
    normal: Vec3f,
    material: *_material,
};

fn random_unit_disk() Vec3f {
    var p: Vec3f = undefined;
    while (true) {
        var random_vec = vec3(rnd(), rnd(), 0.0);
        p = random_vec.mulScalar(2.0).sub(vec3(1, 1, 0));
        if (p.dot(p) < 1.0) {
            break;
        }
    }
    return p;
}

const Camera = struct {
    const Self = @This();

    lower_left_corner: Vec3f,
    horizontal: Vec3f,
    vertical: Vec3f,
    origin: Vec3f,
    u: Vec3f,
    v: Vec3f,
    w: Vec3f,
    lens_radius: f32,

    pub fn new(lookfrom: Vec3f, lookat: Vec3f, vup: Vec3f, vfov: f32, aspect: f32, aperture: f32, focus_dist: f32) Self {
        var theta = vfov * PI / 180.0;
        var half_height = std.math.tan(theta / 2.0);
        var half_width = aspect * half_height;

        var w = lookfrom.sub(lookat).unit_vector();
        var u = vup.cross(w).unit_vector();
        var v = w.cross(u);

        return Camera{
            .lower_left_corner = lookfrom.sub(u.mulScalar(half_width * focus_dist))
                .sub(v.mulScalar(half_height * focus_dist))
                .sub(w.mulScalar(focus_dist)),
            .horizontal = u.mulScalar(half_width * 2 * focus_dist),
            .vertical = v.mulScalar(half_height * 2 * focus_dist),
            .origin = lookfrom,
            .lens_radius = aperture / 2.0,
            .w = w,
            .u = u,
            .v = v,
        };
    }

    pub fn get_ray(c: Self, s: f32, t: f32) Ray {
        var rd = random_unit_disk().mulScalar(c.lens_radius);
        var offset = c.u.mulScalar(rd.x).add(c.v.mulScalar(rd.y));
        return Ray.new(c.origin.add(offset), c.lower_left_corner
            .add(c.horizontal.mulScalar(s))
            .add(c.vertical.mulScalar(t))
            .sub(c.origin)
            .sub(offset));
    }
};

const Sphere = struct {
    const Self = @This();

    center: Vec3f,
    radius: f32,
    material: _material,

    pub fn new(center: Vec3f, radius: f32, material: _material) Self {
        return Sphere{
            .center = center,
            .radius = radius,
            .material = material,
        };
    }

    pub fn hit(self: *Self, r: *Ray, t_min: f32, t_max: f32, rec: *HitRecord) bool {
        var oc = r.origin().sub(self.center);
        var a = r.direction().dot(r.direction());
        var b = oc.dot(r.direction());
        var c = oc.dot(oc) - self.radius * self.radius;
        var discriminant = b * b - a * c;
        if (discriminant > 0) {
            var temp = (-b - sqrt(b * b - a * c)) / a;
            if (temp < t_max and temp > t_min) {
                rec.t = temp;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = (rec.p.sub(self.center)).mulScalar(1 / self.radius);
                rec.material = &self.material;
                return true;
            }
            temp = (-b + sqrt(b * b - a * c)) / a;
            if (temp < t_max and temp > t_min) {
                rec.t = temp;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = (rec.p.sub(self.center)).mulScalar(1 / self.radius);
                rec.material = &self.material;
                return true;
            }
        }
        return false;
    }
};

fn HitableList(comptime T: type) type {
    return struct {
        const Self = @This();

        list: []T,
        index: usize,
        capacity: usize,

        pub fn new(comptime capacity: usize) Self {
            var memory = std.ArrayList(T).init(&GlobalDirectAllocator.allocator);
            _ = memory.resize(capacity);
            var result = Self{
                .list = memory.toOwnedSlice(),
                .index = 0,
                .capacity = capacity,
            };

            return result;
        }

        pub fn add(self: *Self, item: T) void {
            if (self.index >= self.list.len) {
                warn("Container capacity exceeded!\n");
                return;
            }
            self.list[self.index] = item;
            self.index += 1;
        }

        pub fn hit(self: *Self, r: *Ray, t_min: f32, t_max: f32, rec: *HitRecord) bool {
            var temp_rec: HitRecord = undefined;
            var hit_anything = false;
            var closest_so_far = t_max;
            for (self.list) |*item| {
                if (item.hit(r, t_min, closest_so_far, &temp_rec)) {
                    hit_anything = true;
                    closest_so_far = temp_rec.t;
                    rec.* = temp_rec;
                }
            }
            return hit_anything;
        }
    };
}

fn random_scene() HitableList(Sphere) {
    const n: usize = 501;
    var list = HitableList(Sphere).new(n);
    list.add(Sphere.new(vec3(0, -1000, 0), 1000, Material(MaterialType.Lambert, vec3(0.5, 0.5, 0.5), 0.0, 0.0)));
    var a: i32 = -11;
    while (a < 11) : (a += 1) {
        var b: i32 = -11;
        while (b < 11) : (b += 1) {
            var choose_mat: f32 = rnd();
            var center: Vec3f = vec3(@intToFloat(f32, a) + 0.9 * rnd(), 0.2, @intToFloat(f32, b) + 0.9 * rnd());
            if (center.sub(vec3(4, 0.2, 0)).norm() > 0.9) {
                if (choose_mat < 0.8) { // Diffuse
                    var rnd1 = rnd();
                    var rnd2 = rnd();
                    var rnd3 = rnd();
                    var rnd4 = rnd();
                    var rnd5 = rnd();
                    var rnd6 = rnd();
                    list.add(Sphere.new(center, 0.2, Material(MaterialType.Lambert, vec3(rnd1 * rnd2, rnd3 * rnd4, rnd5 * rnd6), 0.0, 0.0)));
                } else if (choose_mat < 0.95) { // Metal
                    var rnd1 = rnd();
                    var rnd2 = rnd();
                    var rnd3 = rnd();
                    var rnd4 = rnd();
                    list.add(Sphere.new(center, 0.2, Material(MaterialType.Metallic, vec3(0.5 * (1 + rnd1), 0.5 * (1 + rnd2), 0.5 * (1 + rnd3)), rnd4, 0.0)));
                } else { // Glass
                    list.add(Sphere.new(center, 0.2, Material(MaterialType.Dielectric, vec3(0.0, 0.0, 0.0), 0.0, 1.5)));
                }
            }
        }
    }
    list.add(Sphere.new(vec3(0, 1, 0), 1.0, Material(MaterialType.Dielectric, vec3(0.0, 0.0, 0.0), 0.0, 1.5)));
    list.add(Sphere.new(vec3(-5, 1, 0), 1.0, Material(MaterialType.Lambert, vec3(0.4, 0.2, 0.1), 0.0, 0.0)));
    list.add(Sphere.new(vec3(4, 1, 0), 1.0, Material(MaterialType.Metallic, vec3(0.7, 0.6, 0.5), 0.0, 0.0)));
    return list;
}

fn random_unit_sphere() Vec3f {
    var p: Vec3f = undefined;
    while (true) {
        p = vec3(rnd(), rnd(), rnd()).mulScalar(2.0).sub(vec3(1, 1, 1));
        if (p.squared_length() >= 1.0) {
            break;
        }
    }
    return p;
}

fn refract(v: *const Vec3f, n: *const Vec3f, ni_over_nt: f32, refracted: *Vec3f) bool {
    var uv = v.unit_vector();
    var dt = uv.dot(n.*);
    var discriminant = 1.0 - ni_over_nt * ni_over_nt * (1 - dt * dt);
    if (discriminant > 0.0) {
        refracted.* = (uv.sub(n.mulScalar(dt))).mulScalar(ni_over_nt).sub(n.mulScalar(sqrt(discriminant)));
        return true;
    } else {
        return false;
    }
}

// Real glass reflectivity varies with angle. We'll use simple polynomial
// approximation by Christophe Schlick
fn schlick(cosine: f32, ref_idx: f32) f32 {
    var r0 = (1 - ref_idx) / (1 + ref_idx);
    r0 = r0 * r0;
    return r0 + (1 - r0) * pow(f32, 1 - cosine, 5.0);
}

// ===================================================================================================
//                                            MATERIALS
// ===================================================================================================

const _material = struct {
    const Self = @This();

    albedo: Vec3f,
    fuzz: f32, // Surface bumpyness
    ref_idx: f32, // Index of refraction

    scatter_function: *const fn (*const Self, *Ray, *HitRecord, *Vec3f, *Ray) bool,

    pub fn scatter(self: Self, r_in: *Ray, rec: *HitRecord, attenuation: *Vec3f, scattered: *Ray) bool {
        return self.scatter_function.*(&self, r_in, rec, attenuation, scattered);
    }
};

const MaterialType = enum {
    Lambert,
    Metallic,
    Dielectric,
};

fn Material(material_type: MaterialType, albedo: Vec3f, fuzz: f32, ref_idx: f32) _material {
    var scattering_function = switch (material_type) {
        MaterialType.Lambert => &lambert_scatter,
        MaterialType.Metallic => &metallic_scatter,
        MaterialType.Dielectric => &dielectric_scatter,
        else => &lambert_scatter,
    };
    return _material{
        .albedo = albedo,
        .scatter_function = scattering_function,
        .fuzz = fuzz,
        .ref_idx = ref_idx,
    };
}

fn lambert_scatter(self: *const _material, r_in: *Ray, rec: *HitRecord, attenuation: *Vec3f, scattered: *Ray) bool {
    _ = r_in;
    var target = rec.p.add(rec.normal).add(random_unit_sphere());
    scattered.* = Ray.new(rec.p, target.sub(rec.p));
    attenuation.* = self.albedo;
    return true;
}

fn metallic_scatter(self: *const _material, r_in: *Ray, rec: *HitRecord, attenuation: *Vec3f, scattered: *Ray) bool {
    var reflected = r_in.direction().reflect(rec.normal);
    scattered.* = Ray.new(rec.p, reflected.add(random_unit_sphere().mulScalar(self.fuzz)));
    attenuation.* = self.albedo;
    return scattered.direction().dot(rec.normal) > 0;
}

fn dielectric_scatter(self: *const _material, r_in: *Ray, rec: *const HitRecord, attenuation: *Vec3f, scattered: *Ray) bool {
    var outward_normal: Vec3f = undefined;
    var reflected = r_in.direction().reflect(rec.normal);
    var ni_over_nt: f32 = undefined;
    attenuation.* = vec3(1.0, 1.0, 1.0);
    var refracted: Vec3f = undefined;
    var reflect_prob: f32 = undefined;
    var cosine: f32 = undefined;

    if (r_in.direction().dot(rec.normal) > 0) {
        outward_normal = rec.normal.negate();
        ni_over_nt = self.ref_idx;
        cosine = self.ref_idx * r_in.direction().dot(rec.normal) / r_in.direction().norm();
    } else {
        outward_normal = rec.normal;
        ni_over_nt = 1.0 / self.ref_idx;
        cosine = -r_in.direction().dot(rec.normal) / r_in.direction().norm();
    }

    if (refract(&r_in.direction(), &outward_normal, ni_over_nt, &refracted)) {
        reflect_prob = schlick(cosine, self.ref_idx);
    } else {
        reflect_prob = 1.0;
    }

    if (rnd() < reflect_prob) {
        scattered.* = Ray.new(rec.p, reflected);
    } else {
        scattered.* = Ray.new(rec.p, refracted);
    }
    return true;
}
